<?php

namespace UflAs\Session;

use SessionHandlerInterface as DefaultSessionHandlerInterface;

/**
 * Interface SessionHandlerInterface
 * @package UflAs\Session
 */
interface SessionHandlerInterface extends DefaultSessionHandlerInterface
{
    // empty
}