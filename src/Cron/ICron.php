<?php

namespace UflAs\Cron;

/**
 * Interface ICron
 * @package UflAs\Cron
 */
interface ICron
{
    public function execute();
}